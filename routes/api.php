<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post("/register", "UserController@register");
Route::get("/state/{id}", "UserController@laststate");

Route::get("/profile/{id}", "PeopleController@profile");
Route::get("/users", "PeopleController@userlist");
Route::get("/user/{id}/meetings", "MeetingController@getList");
Route::get("/user/{id}/contacts", "PeopleController@contacts");
Route::post('/user/{id}/set/guide', 'PeopleController@setAsGuide');
Route::post("/user/{id}/contacts/add/{contactid}", "PeopleController@addContact");

Route::post("/updatelocation", "LocationUpdaterController@update");
Route::post("/meeting/create", "MeetingController@create");

Route::get("/meeting/{id}", "MeetingController@get");
Route::post("/meeting/positions", "MeetingController@getLocations");
Route::get("/meeting/{id}/track/{userid}", "MeetingController@getTracks");
Route::get("/meeting/{id}/finish", "MeetingController@finish");

Route::get("/destinations", 'TourDestinationController@get');
Route::get("/destination/{user}/get", 'TourDestinationController@getByGuide');
Route::post('/destination', 'TourDestinationController@create');
Route::put('/destination/{id}', 'TourDestinationController@save');

Route::get("/groupof/{user}", 'TourGroupController@getByUser');
Route::get('/groups/{initiator}', 'TourGroupController@getByInitiator');
Route::post('/groups/{initiator}/create', 'TourGroupController@createByInitiator');
Route::get('/groups/{group}/members', 'TourGroupController@getMemberByGroup');
Route::get('/groups/{group}/chats', 'TourGroupController@getChats');
Route::get('/groups/{group}/locations', 'TourGroupController@getMemberLocations');
Route::get('/groups/{group}/info', 'TourGroupController@getGroupInfo');
Route::delete('/groups/{group}', 'TourGroupController@deleteGroup');

Route::post('/groups/{group}/destination/{destination}', 'TourGroupController@setTourDestination');
Route::delete('/groups/{group}/destination', 'TourGroupController@clearTourDestination');

Route::post('/group/chat', 'TourGroupController@sendChat');
Route::get('/guide/accept/{user}', 'PeopleController@acceptGuide');
