<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    function register(Request $r){
        $user = \App\Models\User::where("email",$r->input("email"))->first();
        if($user !== null){            
            $user->update($r->only('google_auth_code', 'firstname', 'lastname', 'email', 'phone', 'token','photo_url'));
            $user->token = str_random(150);
            $user->save();
        }
        else{
            $user = \App\Models\User::create($r->only('google_auth_code', 'firstname', 'lastname', 'email', 'phone', 'token','photo_url'));
            $user->token = str_random(150);
            $user->save();
        }

        return response()->json($user);
    }
    function laststate($id){
        $user = \App\Models\User::find($id);
        if($user === null){
            return response()->json(null,404);
        }

        $meeting = $user->meetings()->latest()->first();
        if($meeting !== null && $meeting->status === "active"){
            $meeting->meetingMembers->load("user");
            return response()->json($meeting);
        }

        $inMeetings = $user->inmeeting()->latest()->first();
        if($inMeetings !== null){
            $meetingstatus = $inMeetings->meeting;
            
            if($meetingstatus->status === "active"){
                $meetingstatus->meetingMembers->load("user");
                return response()->json($meetingstatus);
            }
        }

        return response()->json(null,304);
    }
}
