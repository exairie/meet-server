<?php

namespace App\Http\Controllers;

use App\Models\UserPosition;
use Illuminate\Http\Request;

class LocationUpdaterController extends Controller
{
    function update(Request $r)
    {
        $userid = $r->input('user_id');
        UserPosition::where('user_id', $userid)->delete();
        $pos = \App\Models\UserPosition::create($r->all());

        return response()->json($pos, 201);
    }
}
