<?php

namespace App\Http\Controllers;

use App\Models\TourDestination;
use App\Models\User;
use Illuminate\Http\Request;

class TourDestinationController extends Controller
{
    function get(Request $r)
    {
        $data = TourDestination::with(['userCreator'])->get();

        return response()->json($data);
    }
    function getByGuide(Request $r, User $user)
    {
        $data = $user->tourDestinations;

        return response()->json($data);
    }
    function create(Request $r)
    {
        $create = TourDestination::create($r->all());

        return response()->json($create);
    }
    function save(Request $r, $id)
    {
        $update = TourDestination::where('id', $id)
            ->update($r->all());
        return response()->json($update);
    }
}
