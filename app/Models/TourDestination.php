<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property integer $creator
 * @property string $destination_name
 * @property string $description
 * @property float $latitude
 * @property float $longitude
 * @property float $radius
 * @property string $geofences
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property TourGroup[] $tourGroups
 */
class TourDestination extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['creator', 'destination_name', 'description', 'latitude', 'longitude', 'radius', 'geofences', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userCreator()
    {
        return $this->belongsTo('App\Models\User', 'creator');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tourGroups()
    {
        return $this->hasMany('App\Models\TourGroup', 'active_waypoint');
    }
}
