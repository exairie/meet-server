<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Guide Registration</title>
</head>

<body>
    Hi admin, {{ $user->firstname }} is registering as a Tour Guide<br />
    <a href="http://devserver.datditdut.com/meet/public/api/guide/accept/{{ $user->id }}">Click here to accept or go to this link</a>
    http://devserver.datditdut.com/meet/public/api/guide/accept/{{ $user->id }}
</body>

</html>